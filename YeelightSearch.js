import EventEmitter from 'events'
import {Client} from 'node-ssdp'

import Yeelight from './Yeelight'

class YeelightSearch extends EventEmitter {
    constructor() {
        super()

        this.yeelights = []
        // Setting the sourcePort ensures multicast traffic is received
        this.client = new Client({sourcePort: 1982, ssdpPort: 1982})

        this.client.on('response', data => this.addLight(data))
        // Register devices that sends NOTIFY to multicast address too
        this.client.on('advertise-alive', data => this.addLight(data))

        this.client.search('wifi_bulb')
    }

    addLight(lightdata) {
        let yeelight = this.yeelights.find(item => item.getId() === lightdata.ID)
        if (!yeelight) {
            yeelight = new Yeelight(lightdata)
            this.yeelights.push(yeelight)
            this.emit('found', yeelight)
        }
    }

    getYeelights() {
        return this.yeelights
    }

    getYeelightById(id) {
        return this.yeelights.find(item => item.getId() === id)
    }

    refresh() {
        this.client.search('wifi_bulb')
    }
}

module.exports = YeelightSearch
