import express from 'express'
import YeelightSearch from './YeelightSearch'
import io from 'socket.io-client'

// Set server port. If not defined in an environment variable, the default is used.
const port = process.env.PORT || 5001
//const SOCKET_URL = 'http://192.168.1.121:3000'
const SOCKET_URL = 'http://practicapp.be:3000'

// Initialize Express app
const app = express()
app.use(express.static('static'))

// Constants
const LAMP_DISCOVER_PERIOD_MS = 30*1000
const VERBOSE = false

// Yeelight API
const yeelightSearch = new YeelightSearch()
yeelightSearch.refresh()

// Socket.IO client
const socket = io(SOCKET_URL)

// Lamp search interval
setInterval(() => {
    try {
        yeelightSearch.refresh()
        VERBOSE && console.log('Searching for lamps. Current amount of lamps: ', yeelightSearch.getYeelights().length)
    } catch (e) {
        console.error("Something went wrong when discovering: ", e)
    }
}, LAMP_DISCOVER_PERIOD_MS)

yeelightSearch.on('found', async (lamp) => {
    lamp.on('notification', async () => {
        const state = await lamp.getState()
        socket.emit('newLampState', state)
    })
})

// Socket listener
socket.on('lampState', (data) => {
    console.log(`[Socket] Got new state: `, JSON.stringify(data))
    yeelightSearch.getYeelights().forEach((lamp) => {
        lamp.sync(data)
    })
})

// Start Express App
app.listen(port, () => console.log(`Started YeelightSync Server! Listening on port ${port}`));
