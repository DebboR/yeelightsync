import net from 'net'
import url from 'url'
import debug from 'debug'
import EventEmitter from 'events'
import {uppercase} from "./utils"

const REQUEST_TIMEOUT_MS = 1000

const COLOR_MODES = {
    rgb: 'RGB mode',
    colorTemp: 'Color temperature mode',
    hsv: 'HSV mode'
}
const YEELIGHT_PARAMETERS = [
    {
        name: 'power',
        parameterName: 'power',
        type: 'bool',
        trueValue: 'on',
        setFunction: async (prevData, obj, input, smooth=false, time=1000) => {
            return await obj.sendRequest('set_power', [input, smooth ? 'smooth' : 'sudden', time])
        }
    },
    {
        name: 'brightness',
        parameterName: 'bright',
        type: 'integer',
        minValue: 0,
        maxValue: 100,
        setFunction: async (prevData, obj, input, smooth=false, time=1000) => {
            return await obj.sendRequest('set_bright', [input, smooth ? 'smooth' : 'sudden', time])
        }
    },
    {
        name: 'colorTemperature',
        parameterName: 'ct',
        type: 'integer',
        minValue: 1700,
        maxValue: 6500,
        setFunction: async (prevData, obj, input, smooth=false, time=1000) => {
            return await obj.sendRequest('set_ct_abx', [input, smooth ? 'smooth' : 'sudden', time])
        }
    },
    {
        name: 'RGB',
        parameterName: 'rgb',
        type: 'integer',
        minValue: 1,
        maxValue: 16777215,
        setFunction: async (prevData, obj, input, smooth=false, time=1000) => {
            return await obj.sendRequest('set_rgb', [input, smooth ? 'smooth' : 'sudden', time])
        }
    },
    {
        name: 'hue',
        parameterName: 'hue',
        type: 'integer',
        minValue: 0,
        maxValue: 359,
        setFunction: async (prevData, obj, input, smooth=false, time=1000) => {
            return await obj.sendRequest('set_hsv', [input, prevData.saturation, smooth ? 'smooth' : 'sudden', time])
        }
    },
    {
        name: 'saturation',
        parameterName: 'sat',
        type: 'integer',
        minValue: 0,
        maxValue: 100,
        setFunction: async (prevData, obj, input, smooth=false, time=1000) => {
            return await obj.sendRequest('set_hsv', [prevData.hue, input, smooth ? 'smooth' : 'sudden', time])
        }
    },
    {
        name: 'colorMode',
        parameterName: 'color_mode',
        type: 'enum',
        values: {1: COLOR_MODES.rgb, 2: COLOR_MODES.colorTemp, 3: COLOR_MODES.hsv}
    },
    {
        name: 'flowing',
        parameterName: 'flowing',
        type: 'bool',
        trueValue: 1,
        falseValue: 0,
        setFunction: async (prevData, obj, input) => {
            if(input)
                return await obj.sendRequest('start_cf', [prevData.flowParameters])
            else
                return await obj.sendRequest('stop_cf', [])
        }

    },
    {
        name: 'flowParameters',
        parameterName: 'flow_params',
        type: 'string',
        setFunction: async (prevData, obj, input) => {
            if(prevData.flowing)
                return await obj.sendRequest('start_cf', [input])
        }
    },
    {
        name: 'musicModeEnabled',
        parameterName: 'music_on',
        type: 'bool',
        trueValue: 1
    },
    {
        name: 'name',
        parameterName: 'name',
        type: 'string',
        setFunction: async (prevData, obj, input) => {
            return await obj.sendRequest('set_name', [input])
        }
    }
]

export default class Yeelight extends EventEmitter {
    constructor(data) {
        super()

        if (typeof data === 'undefined' || typeof data !== 'object')
            throw new Error('Options are needed')

        const parsedUri = url.parse(data.LOCATION)
        if (parsedUri.protocol !== 'yeelight:')
            throw new Error(`${parsedUri.protocol} is not supported`)

        this.id = data.ID
        this.name = data.NAME
        this.model = data.MODEL
        this.port = parsedUri.port
        this.hostname = parsedUri.hostname
        this.supports = data.SUPPORT.split(' ')

        this.requestCounter = 1
        this.log = debug(`Yeelight-${this.name}`)

        this.socket = new net.Socket()
        this.socket.on('data', this.dataHandler.bind(this))
        this.socket.connect(this.port, this.hostname, () => {
            this.log(`Connected to ${this.name} ${this.hostname}:${this.port}`)
            this.emit('connected')
        })

        this.generateStateAndFunctions()

        this.on('connected', async () => {
            await this.getFullState()
        })
    }

    async getFullState(){
        try {
            const parameterNames = YEELIGHT_PARAMETERS.map((parameter) => parameter.parameterName)
            const response = await this.sendRequest('get_prop', parameterNames)
            if (response !== undefined) {
                parameterNames.forEach((name, index) => {
                    this.handleStateParameter(name, response[index])
                })
            }
        } catch (e) {
            this.log('Error while getting full state: ', e)
        }
    }

    generateStateAndFunctions() {
        // Generate state object
        this.state = {}
        YEELIGHT_PARAMETERS.forEach((parameter) => {
            this.state[parameter.name] = null
        })

        // Generate NOTIFICATION message handler
        this.on('notification', (message) => {
            try {
                if (message.method === 'props')
                    Object.entries(message.params).forEach(([key, value]) => this.handleStateParameter(key, value))
                console.log('New state:', this.state)
            } catch (e) {
                this.log('Error while handling notification: ' + e.toString())
            }
        })

        // Generate setting functions
        YEELIGHT_PARAMETERS.filter((parameter) => (parameter.setFunction !== undefined)).forEach((parameter) => {
            this['set' + uppercase(parameter.name)] = (args) => parameter.setFunction(this.state, this, ...args)
        })
    }

    handleStateParameter(key, value) {
        const parameterDefinition = YEELIGHT_PARAMETERS.find((parameter) => parameter.parameterName === key)
        if(parameterDefinition !== undefined){
            switch (parameterDefinition.type) {
                case 'bool':
                    this.state[parameterDefinition.name] = (value === parameterDefinition.trueValue)
                    break
                case 'integer':
                    if(value > parameterDefinition.maxValue || value < parameterDefinition.minValue)
                        this.log(`Got value for '${parameterDefinition.name}' which is out of bounds!`)
                    else
                        this.state[parameterDefinition.name] = parseInt(value)
                    break
                case 'string':
                    this.state[parameterDefinition.name] = value
                    break
                case 'enum':
                    const enumValue = parameterDefinition.values[value]
                    if(enumValue === undefined)
                        this.log(`Got value for '${parameterDefinition.name}' which is not known!`)
                    else
                        this.state[parameterDefinition.name] = enumValue
                    break
            }
        } else {
            this.log(`Parameter ${key} is undefined!`)
        }
    }

    sendRequest(method, params) {
        return new Promise((resolve, reject) => {
            if (!this.supports.includes(method)) {
                reject(new Error(`Unsupported method: ${method}`))
                return
            }

            const currentId = this.requestCounter
            const request = JSON.stringify({
                method,
                params,
                id: currentId,
            })
            this.log(`Sending request: ${request}`);

            this.socket.write(`${request}\r\n`, (error) => {
                if (error) {
                    reject(error)
                    return
                }

                // Response timeout
                const timeoutInterval = setTimeout(() => {
                    reject({
                        err: "Timeout"
                    })
                }, REQUEST_TIMEOUT_MS)

                // Listen for response
                const responseListener = (id, res) => {
                    if(id === currentId) {
                        this.removeListener('response', responseListener)
                        clearInterval(timeoutInterval)
                        resolve(res)
                    }
                }

                // Listener timeout
                setTimeout(() => {
                    this.removeListener('response', responseListener)
                }, REQUEST_TIMEOUT_MS)

                this.on('response', responseListener)
            })
            this.requestCounter += 1
        })
    }

    dataHandler(response) {
        try {
            const json = JSON.parse(response);
            const id = json.id;
            const result = json.result;

            if (!id) {
                this.log(`Got response without id: ${response.toString().replace(/\r\n/, '')}`);
                this.emit('notification', json);
                return;
            }

            this.log(`Got response: ${response.toString().replace(/\r\n/, '')}`);

            if (json && json.error) {
                const error = new Error(json.error.message);
                error.code = json.error.code;
                this.emit('error', id, error);
            } else {
                this.emit('response', id, result);
            }
        } catch (ex) {
            this.emit('error', ex);
        }
    }

    getId() {
        return this.id;
    }

    getValues(...props) {
        return this.sendRequest('get_prop', props);
    }

    setScene(params) {
        return this.sendRequest('set_scene', params);
    }

    getState(){
        return this.state
    }

    checkIfDifferent(otherState, parameterNames){
        let result = false
        parameterNames.forEach(name => {
            if(this.state[name] !== otherState[name])
                result = true
        })
        return result
    }

    async sync(otherState){
        if(otherState.power){
            if(otherState.flowing && this.checkIfDifferent(otherState, ['power', 'flowing', 'flowParameters'])){
                const flowParamArray = otherState.flowParameters.split(',')
                await this.setScene(['cf', parseInt(flowParamArray[0]), parseInt(flowParamArray[1]), flowParamArray.slice(2).join(',')])
            } else {
                switch (otherState.colorMode) {
                    case(COLOR_MODES.rgb):
                        this.checkIfDifferent(otherState, ['power', 'RGB', 'brightness', 'colorMode']) && await this.setScene(['color', otherState.RGB, otherState.brightness])
                        break
                    case(COLOR_MODES.colorTemp):
                        this.checkIfDifferent(otherState, ['power', 'colorTemperature', 'brightness', 'colorMode']) && await this.setScene(['ct', otherState.colorTemperature, otherState.brightness])
                        break
                    case(COLOR_MODES.hsv):
                        this.checkIfDifferent(otherState, ['power', 'hue', 'saturation', 'brightness', 'colorMode']) && await this.setScene(['hsv', otherState.hue, otherState.saturation, otherState.brightness])
                        break
                }
            }
        } else {
            this.checkIfDifferent(otherState, ['power']) && await this.setPower(false)
        }
    }
}
